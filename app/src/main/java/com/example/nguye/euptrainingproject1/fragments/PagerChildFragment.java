package com.example.nguye.euptrainingproject1.fragments;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nguye.euptrainingproject1.R;

public class PagerChildFragment extends Fragment implements View.OnClickListener {

    int imageId[] = {R.drawable.f1, R.drawable.f2, R.drawable.f3, R.drawable.f4 };
    String nameText[] = {"Naruto", "OnePie", "One Punch Man", "Dragon Ball"};
    int pos = 0;
    private AnimatorSet showFrontAnim = new AnimatorSet();
    private AnimatorSet showBackAnim = new AnimatorSet();

    CardView mCardFront, mCardBack;

    public void setPostion(int position){
        pos = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_animation, container, false);
        ((ImageView)mView.findViewById(R.id.image)).setImageResource(imageId[pos]);
        ((TextView) mView.findViewById(R.id.movie_name)).setText(nameText[pos]);
        mCardFront = (CardView) mView.findViewById(R.id.front);
        mCardFront.setOnClickListener(this);
        mCardBack = (CardView) mView.findViewById(R.id.back);
        mCardBack.setOnClickListener(this);
        setViewAnimation();
        return mView;
    }

    private void setViewAnimation() {
        mCardBack.setVisibility(View.GONE);

        AnimatorSet leftIn   = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.anim.flip_left_in);
        AnimatorSet rightOut = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.anim.flip_right_out);
        AnimatorSet leftOut  = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.anim.flip_left_out);
        AnimatorSet rightIn  = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.anim.flip_right_in);

        leftIn.setTarget(mCardFront);
        leftOut.setTarget(mCardBack);
        showFrontAnim.playTogether(leftIn, leftOut);
        showFrontAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCardBack.setVisibility(View.GONE);
                mCardBack.setClickable(true);
                mCardFront.setClickable(true);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                mCardFront.setClickable(false);
                mCardBack.setClickable(false);
            }
        });

        rightIn.setTarget(mCardBack);
        rightOut.setTarget(mCardFront);
        showBackAnim.playTogether(rightIn, rightOut);
        showBackAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCardFront.setVisibility(View.GONE);
                mCardBack.setClickable(true);
                mCardFront.setClickable(true);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                mCardFront.setClickable(false);
                mCardBack.setClickable(false);
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                mCardFront.setVisibility(View.VISIBLE);
                showFrontAnim.start();
                break;
            case R.id.front:
                mCardBack.setVisibility(View.VISIBLE);
                showBackAnim.start();
                break;
            default:
                break;
        }
    }

}
