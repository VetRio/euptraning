package com.example.nguye.euptrainingproject1.utils;

/**
 * Created by nguye on 3/15/2016.
 */
public interface Constant {

    String LAYOUT_STRING = "Layout";
    int LAYOUT_POS = 0;

    String INCONTROL_STRING = "Input control";
    int INCONTROL_POS = 1;

    String INEVENT_STRING = "Input event";
    int INEVENT_POS = 2;

    String DIALOG_STRING = "Dialog";
    int DIALOG_POS = 3;

    String NOTI_STRING = "Notification";
    int NOTI_POS = 4;

    String TOAST_STRING = "Toast";
    int TOAST_POS = 5;

    String ASYNTASK_STRING = "Asyntask";
    int ASYNTASK_POS = 6;

    String JSON_STRING = "Json";
    int JSON_POS = 7;

    String PAGE_STRING = "Pager";
    int PAGE_POS = 8;

    String MAIN_LIST[] = {LAYOUT_STRING, INCONTROL_STRING, INEVENT_STRING, DIALOG_STRING, NOTI_STRING, TOAST_STRING, ASYNTASK_STRING, JSON_STRING, PAGE_STRING};


}
