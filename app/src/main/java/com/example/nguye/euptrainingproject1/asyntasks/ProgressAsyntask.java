package com.example.nguye.euptrainingproject1.asyntasks;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

import java.util.Timer;

/**
 * Created by nguye on 3/18/2016.
 */
public class ProgressAsyntask extends AsyncTask<Void, Integer, Void>{

    private ProgressBar mProgressBar;
    public ProgressAsyntask(ProgressBar progressBar){
        super();
        mProgressBar = progressBar;
    }
    @Override
    protected void onPreExecute() {
        mProgressBar.setVisibility(View.VISIBLE);
    }
    @Override
    protected Void doInBackground(Void... params) {
        for (int i = 0; i < 100; i++){
            try {
                Thread.sleep(100);
                publishProgress(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    @Override
    protected void onPostExecute(Void result) {
        mProgressBar.setVisibility(View.INVISIBLE);
    }
    @Override
    protected void onProgressUpdate(Integer... values) {
        mProgressBar.setProgress(values[0]);
    }
}
