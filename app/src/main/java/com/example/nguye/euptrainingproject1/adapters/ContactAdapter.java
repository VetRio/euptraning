package com.example.nguye.euptrainingproject1.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.nguye.euptrainingproject1.R;
import com.example.nguye.euptrainingproject1.asyntasks.JsonDataLoader;
import com.example.nguye.euptrainingproject1.models.Contact;

import java.util.List;

/**
 * Created by nguye on 3/18/2016.
 */
public class ContactAdapter extends ArrayAdapter {
    private LayoutInflater mLayoutInflater = null;
    private List<Contact> mContacts;
    public ContactAdapter(Context context, List<Contact> list) {
        super(context, 0, list);
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContacts = list;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.item_json_contact, null);
        }
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView mail = (TextView) convertView.findViewById(R.id.mail);
        name.setText(mContacts.get(position).name);
        mail.setText(mContacts.get(position).email);
        return convertView;
    }
}
